module.exports = {
  toUpper: function (task) {
    var db = this.using("db");
    task.progress("in progress");
    task.end(task.payload.str.toUpperCase());
  },
};
